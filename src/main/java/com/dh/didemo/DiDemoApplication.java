package com.dh.didemo;

import com.dh.didemo.controller.ConstructorBasedController;
import com.dh.didemo.controller.GetterBasedController;
import com.dh.didemo.controller.MyController;
import com.dh.didemo.controller.PropertyBasedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DiDemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController");
        controller.hello();

        System.out.println(context.getBean(PropertyBasedController.class).sayHello());
        System.out.println(context.getBean(GetterBasedController.class).sayHello());
        System.out.println(context.getBean(ConstructorBasedController.class).sayHello());

    }
}
